function [F, Achar, Bchar, matchScr, mismatchScr, gapScr, AlignmentA, AlignmentB, matches, gaps, route] = alignment(a, b, F, Achar, Bchar, matchScr, mismatchScr, gapScr, route)

    % alignment(); func. responsible for finding the optimal route of
    % both sequences alignment;
    % -> creates a '0's and '1's matrix with the apppropriate cells marked
    %    as the optimal route/apath;
    % -> counts NFO DATA variables;

    AlignmentA = "";            % alignment A seq init;
    AlignmentB = "";            % alignment B seq init;
    
    planeSize = [a, b];
    route = zeros(planeSize);   % 'optimal path' zeros matrix init; will be filled with '1' on specific F(i,j) cell where the optimal route is set;

    i = a;                      % setting the for loop starting cell to the (F(end:end)); 
    j = b;                      % in order to begin the optimal routing 
                                % going: F(nxm) -> F(1:1) == 0;
                                % 'a' and 'b' stands for the lengths of the
                                % matrix (for typical standards could be
                                % 'n'x'm');
    
    matches = 0;                % NFO DATA - matches sum init;
    mismatches = 0;             % NFO DATA - mismatches sum init;
    gaps = 0;                   % NFO DATA - gaps sum init;
    
    route(end,end) = 1;         % setting the route ('0''s-'1''s) matrix initial step;
    % ---------------------------------------------------------------------
    
    while (i > 1) && (j > 1)    % execute the loop until 'i' or 'j' from the F(a,b) reaches value '1', which is equivalent in reaching X/Y plot axis -> (F(1:b) or F(a:1)) of the matrix;

        if Achar(i)==Bchar(j)             % init and assign the 'currentMatchScr' in order to distinguish
            currentMatchScr = matchScr;   % diagonal scoring step from match or MISmatch;
            matches = matches +1;         % NFO DATA - matches sum unitary rise;
        else                              %
            currentMatchScr = mismatchScr;%
            mismatches = mismatches +1;   % NFO DATA - MISmatches sum unitary rise;
        end

        if (i > 0 && j > 0) && F(i,j) == F(i-1,j-1) + currentMatchScr % if the current F(i,j) matrix cell value is equal to 
                                                                      % the neighbouring diagonal (upper-left) + beforehand defined match/MISmatch score 
                                                                      % go for 'synonymic/nonsynonymic SUBSTITUTION' way;
            AlignmentA = Achar(i) + AlignmentA;                       % add to A alignment current A sequence nucleotide letter;
            AlignmentB = Bchar(j) + AlignmentB;                       % add to B alignment current B sequence nucleotide letter;
            i = i - 1;                                                % go back diagonally within the F matrix;
            j = j - 1;                                                % -||-

            route(i,j)=1;                                             % set current route matrix cell value to '1';

        elseif (i > 0 && F(i,j) == F(i-1,j) + gapScr)                 % analogous case, but this time consider INSERTION: 
                                                                      % occurs when the current F(i,j) matrix cell value is equal to the neighbouring left cell score + gap score
                                                                      % 
            AlignmentA = Achar(i) + AlignmentA;                       % add to A alignment current A sequence nucleotide letter;
            AlignmentB = "_" + AlignmentB;                            % set a GAP current B sequence nucleotide letter;
            i = i - 1;
            gaps = gaps +1;

            route(i,j)=1;

        else                                                          % analogous case, but this time consider DELETION (the only left case):
                                                                      % occurs when the current F(i,j) matrix cell value is equal to the neighbouring upper cell score + gap score
            AlignmentA = "_" + AlignmentA;                            % set a GAP current A sequence nucleotide letter;
            AlignmentB = Bchar(j) + AlignmentB;                       % add to B alignment current B sequence nucleotide letter;
            j = j - 1;
            gaps = gaps +1;

            route(i,j)=1;

        end

    end

    if i > 1    % this is the same exact INSERTION code as before,
                % having different conditions that execute the code
                % only when the upper edge of plot/matrix F(1:b) is reached,
                % but the left isn't yet;

        while (i ~= 1)
            AlignmentA = Achar(i) + AlignmentA;
            AlignmentB = "_" + AlignmentB;
            i = i - 1;
            gaps = gaps +1;

            route(i,j)=1;

        end

    end

    if (j > 1)  % this is the same exact DELETION code as before,
                % having different conditions that execute the code
                % only when the left edge of plot/matrix F(1:b) is reached,
                % but the upper isn't yet;

        while (j ~= 1)

            AlignmentA = "-" + AlignmentA;
            AlignmentB = Bchar(j) + AlignmentB;
            j = j - 1;
            gaps = gaps +1;

            route(i,j)=1;

        end

    end
    
    % ---------------------------------------------------------------------
           
 end