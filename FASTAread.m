function [seqVector1, seqVector2, xAxisTitle, yAxisTitle] = FASTAread()

% FASTAread(); function responsible for:
% 1.) loading the *.fna file type (or similar FASTA text-structured)
%     content;
% 2.) parsing loaded data into a continuous sequence of 
%     nucleotide symbols ('A', 'T', 'G', 'C') 
%     giving in the end one line String of derived data;  

    for step = 1:1:2

        % Path/filename prompt set up;
        
        fileNo = step;
        fileNoStr = strcat(' ', string(fileNo));
        Str1 = 'Choose (';
        Str2 = '). FASTA file';
        title = strcat(Str1, fileNoStr, Str2);
        
        [file] = uigetfile('*.fna', title);

        % FASTA file import;
        fastaContent = importdata(file);
        % Getting rid of 1. FASTA info text line;
        genSeq = fastaContent (2:end, 1);
        % Joining symbol sequence lines;
        genSeqVector = strjoin(genSeq(:));
        % Getting rid of space symbols;
        genSeqStr = regexprep(genSeqVector, '\s+', '');

        % output;
        nfo = fastaContent (1, 1);
        if step==1
        seqVector1 = cellstr(genSeqStr);
        xAxisTitle = string(nfo);
        end
        
        if step==2
        seqVector2 = cellstr(genSeqStr);
        yAxisTitle = string(nfo);
        end
    
    end
    
end

