function [a, b, F, Achar, Bchar, matchScr, mismatchScr, gapScr, mode] = generateMatrix(A, B, matchScr, mismatchScr, gapScr)

    % generate Matrix: to pass: a, b, F, Achar, Bchar, matchScr, mismatchScr, gapScr, mode
    % generateMatrix(A,B); function responsible for generating the base
    %                      matrix passed to 'alignment.m' func.;

    % A = cellstr('ATCCGATCGCGATAGCT'); #script testing value
    % B = cellstr('CGAGGACCGA');        #script testing value

    mode = "";              % NFO DATA - initialize mode String (for afterward 'distance' / 'simiatirty' assign)
    dash = '_';             
    A = strcat(dash, A);
    B = strcat(dash, B);    % adding a init 'gap' at the beginning of both sequences;

    a = length(char(A));
    b = length(char(B));
    
    % planeSize as a matrix containing the zerosPlane size n+1(gap) x m+1(gap); 
    planeSize = [a, b];
    % crucial zerosPlane matrix assign (stores scoring values);
    zerosPlane = zeros(planeSize);

    % algorithm

    F = zerosPlane;
    % matchScr= 1;          #script testing value
    % mismatchScr = -0.5;   #script testing value
    % gapScr = -1;          #script testing value
    % ---------------------------------------------------------------------


    for i=2:1:(a)           % fill the score vertical gap column no.1;
      F(i,1) = gapScr*(i-1);
    end

    for i=2:1:(b)           % fill the score horizontal gap column no.1;
      F(1,i) = gapScr*(i-1);
    end

    Achar = char(A);
    Bchar = char(B);

    for i=2:1:(a)

        for j=2:1:(b)

            if Achar(i)==Bchar(j)
                currentMatch = (F(i-1,j-1) + matchScr);     % assign equivalent score diagonally if the corresponding sequence nucleotides match;
            else                                            %
                currentMatch = (F(i-1,j-1) + mismatchScr);  % -||- MISmatch;
            end                                             %
                                                            %
            currentGapLeft = (F(i-1,j) + gapScr);           % -||- horizontally for possible gap (INSERTION);
            currentGapUp = (F(i,j-1) + gapScr);             % -||- vertically for possible gap (DELETION);

            if matchScr >= mismatchScr && matchScr >= gapScr                % if the 'matching score' from all of the input scoring values is the highest one, optimal path finding will base on minimalization (going down to '0')- set the mode for 'similarity';
                current = max([currentMatch currentGapLeft currentGapUp]);  % assign the MIN score;
                mode = "similarity";                                        %
            elseif matchScr <= mismatchScr && matchScr <= gapScr            % -||- is the lowest -||- optimal path finding will base on maximization (going up to '0'), mode -> 'distance';
                current = min([currentMatch currentGapLeft currentGapUp]);  % assign the MAX score;
                mode="distance";
            end

            F(i,j) = current;  % set current (MIN/MAX) score for current position int he (i,j) F zeroes matrix ;

        end

    end

    % ---------------------------------------------------------------------

end
