function [seqVector1, seqVector2, xAxisTitle, yAxisTitle] = SEQread()

% manualSeqRead(); function responsible for enabling user to manually 
%                  type the nucleotide sequence;

    for step = 1:1:2

        % Path/filename prompt set up;
        fileNo = step;
        fileNoStr = strcat(' ', string(fileNo));
        Str1 = 'Type (';
        Str2 = '). nucleotide sequence: ';
        
        prompt = {strcat(Str1, ' ', fileNoStr, Str2)};
        dlgtitle = 'Sequence';
        dims = [1 100];
        definput = {''};
        seq = inputdlg(prompt,dlgtitle,dims,definput);

        % output;
        if step==1
        seqVector1 = seq;
        end
        
        if step==2
        seqVector2 = seq;
        end
    
    end
    
    xAxisTitle = "2.(B) user input sequence";
    yAxisTitle = "1.(A) user input sequence";
    
end
