function [] = generateHeatMap(F, route, xAxisTitle, yAxisTitle)
    
    % generateHeatMap(); responsible for creating two plots:
    %   1.) the heat plot based on the F(i,j) matrix with scoring values;
    %   2.) the heat plot with data-dependent heat scale with the 'route'
    %       matrix showing the optimal path combined; 

    % Heatmap based on F(j,j) matrix generation;
    set(0, 'DefaultFigureVisible', 'on');
    title = string("Gene sequence-pair global alignment scoring" + newline);
    heat = HeatMap(flipud(F), 'Colormap', parula, 'Title', title, 'XLabel', ...
    string(xAxisTitle), 'YLabel', string(yAxisTitle));
    axis square;
    heat.Annotate = true;
    % ---------------------------------------------------------------------
    
    % close HeatMapObj popping out ('HandleVisibility', 'off'!), but leave the GUI!
    fig_h = permute( findobj( 0, 'Type', 'Figure' ), [2,1] ); 
    for fh = fig_h
        uih = findobj( fh, 'Type', 'uicontrol' );
        if isempty( uih )
            delete( fh );
        end
    end
    % ---------------------------------------------------------------------

    % put the heatmap into the plot fgure, to keep plot manipulation (zoom, drag etc.)
    % and gain the ability to save/export file;
    figHeat = figure('Name', "Nucleotide sequences alignment HeatMap");
    plotheat = plot(heat, figHeat);
    axis square;
    plotheat.XTickLabelMode = 'auto';
    % ---------------------------------------------------------------------
 
    % setting the heatmap with the exact same colormap as imagesc(); into
    % the figure and combine it with the optimal route path plot (red 'o's);
    figure('Name', "Nucleotide sequences optimal alignment path/route + data-dependent heat scale")
    imagesc(F);
    cb = colorbar;
    cb.Label.String = "Score spectrum";
    hold on;
    spy(route, "ro");
    axis square;
    hold on;
    % ---------------------------------------------------------------------
 
end