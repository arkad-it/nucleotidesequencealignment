function [] = saveNFO(F, Achar, Bchar, mode, matchScr, mismatchScr, gapScr, AlignmentA, AlignmentB, matches, gaps)
    
    % saveNFO(); responsible for: creating the alignment process data info
    %            table, and allowing user to save it into the *.txt/*.xls file;

    ln1 = string("# 1: " + Achar);                  % seq1
    ln2 = string("# 2: " + Bchar);                  % seq2
    ln3 = "# Mode: " + mode;                        % mode
    ln4 = "# Match: " + matchScr;                   % match score
    ln5 = "# Mismatch: " + mismatchScr;             % mismatch score
    ln6 = "# Gap: " + gapScr;                       % gap score
    ln7 = "# Score: " + F(end,end);                 % total score
    ln8 = "# Length: " + length(char(AlignmentA));  % total length
    
    idenityPercent = (matches/length(char(AlignmentA))) * 100;
    ln9 = "# Identity: " + matches + "/" + length(char(AlignmentA)) + " (" + idenityPercent + "%)";   % matches/length [%]
    
    gapsPercent = (gaps/length(char(AlignmentA))) * 100;
    ln10 = "# Gaps: " + gaps + "/" + length(char(AlignmentA)) + " (" + gapsPercent + "%)";            % gaps/length [%]
   
    ln11 = AlignmentA;                              % A alignment sequence
    
    spaces = "";                                    % match/mismatch/gap string init;
    charAlignmentA = (char(AlignmentA));
    charAlignmentB = (char(AlignmentB));
    
    for i=1:1:length(char(AlignmentA))              
    spaces = spaces + " ";
    end
    
    charSpaces = char(spaces);
    
    for i=1:1:length(char(AlignmentA))                  % set '|' where match occurs, set 'X' where mismatch/gap occurs
        if charAlignmentA(1, i) == charAlignmentB(1, i)
            charSpaces(1, i) = "|";
        else
            charSpaces(1, i) = "X";
        end
    end
    
    ln12 = string(charSpaces);    % similarity String assign;
    ln13 = AlignmentB;            % B alignment sequence;
    % ---------------------------------------------------------------------
    
    % create dataTable with beforehand assigned lines (ln1 -> ln13);
    dataTable = table(ln1, ln2, ln3, ln4, ln5, ln6, ln7, ln8, ln9, ln10, ln11, ln12, ln13);
    dataTable = table2cell(dataTable);
    dataTable = cell2table(dataTable'); % final String data in columns ready to be generated;
    
    % save file dialog box;
    filter = {'*.txt';'*.xls'}; % available file extensions;
    [filename, pathname] = uiputfile(filter, 'Save alignment analysis NFO'); % box set;
    fullpath = string(pathname) + string(filename);
    
    if string(filename)==""
        writetable(dataTable, fullpath, 'WriteVariableNames',0);  % save file if filename was provided;
                                                                  %
    end
    
 end